package com.emiliedelarue.myapplication.webservice


import com.emiliedelarue.myapplication.objet.Vehicule
import com.emiliedelarue.myapplication.objet.Vehicules
import retrofit2.Call
import retrofit2.http.GET

interface WSInterface {
    // appel get :
    @GET("madrental/get-vehicules.php")
    fun getAllVehicules(): Call<ArrayList<Vehicule>>
}
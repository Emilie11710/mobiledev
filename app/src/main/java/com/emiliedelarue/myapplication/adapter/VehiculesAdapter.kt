package com.emiliedelarue.myapplication.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.emiliedelarue.myapplication.DetailActivity
import com.emiliedelarue.myapplication.MainActivity
import com.emiliedelarue.myapplication.R
import com.emiliedelarue.myapplication.objet.Vehicule
import com.squareup.picasso.Picasso

class VehiculesAdapter ( private var listeVehicules: ArrayList<Vehicule>) :
RecyclerView.Adapter<VehiculesAdapter.VehiculeViewHolder>()
{
    // Crée chaque vue item à afficher :
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehiculeViewHolder
    {
        val viewVehicule = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_vehicule, parent, false)
        return VehiculeViewHolder(viewVehicule)
    }

    // Renseigne le contenu de chaque vue item :
    override fun onBindViewHolder(holder: VehiculeViewHolder, position: Int)
    {
        holder.textViewVehiculeName.text = listeVehicules[position].nom
        holder.textViewVehiculePrice.text = listeVehicules[position].prixjournalierbase.toString() + " € / jour"
        holder.textViewVehiculeCategory.text = "Catégorie CO2 : " + listeVehicules[position].categorieco2

        //pour l'affichage des photos
        Picasso.get()
            .load("http://s519716619.onlinehome.fr/exchange/madrental/images/" + listeVehicules[position].image)
            .into(holder.imageVehicule)

    }
    override fun getItemCount(): Int = listeVehicules.size


    // ViewHolder :
    inner class VehiculeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val textViewVehiculeName: TextView = itemView.findViewById(R.id.vehicule_name)
        val textViewVehiculePrice: TextView = itemView.findViewById(R.id.vehicule_price)
        val textViewVehiculeCategory: TextView = itemView.findViewById(R.id.vehicule_cat)
        val imageVehicule : ImageView = itemView.findViewById(R.id.vehicule_img)

        //listener de click sur Item
        init {
            itemView.setOnClickListener {
                val vehicule = listeVehicules[adapterPosition]
                val intent = Intent(itemView.context, DetailActivity::class.java)
                intent.putExtra("vehicule", vehicule)
                //itemView.context.startActivity(intent)



            }
        }
    }
}

package com.emiliedelarue.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.emiliedelarue.myapplication.adapter.VehiculesAdapter
import com.emiliedelarue.myapplication.objet.Vehicule
import com.emiliedelarue.myapplication.objet.Vehicules
import com.emiliedelarue.myapplication.webservice.RetrofitSingleton
import com.emiliedelarue.myapplication.webservice.WSInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var listeVehicules: ArrayList<Vehicule> = ArrayList()


        //appel au Webservice
        val service = RetrofitSingleton.retrofit.create(WSInterface::class.java)
        val call = service.getAllVehicules()
        call.enqueue(object : Callback<ArrayList<Vehicule>>
        {
            override fun onResponse(call: Call<ArrayList<Vehicule>>, response: Response<ArrayList<Vehicule>>)
            {
                if (response.isSuccessful)
                {
                    listeVehicules.addAll(response.body()!!);
                    Log.d("tag", "${listeVehicules}")
                }
            }
            override fun onFailure(call: Call<ArrayList<Vehicule>>, t: Throwable)
            {
                Log.e("tag", "${t.message}")
            }
        })

        // recyclerView
        val recyclerViewVehicule: RecyclerView = findViewById(R.id.liste_vehicules)

        // à ajouter pour de meilleures performances :
        recyclerViewVehicule.setHasFixedSize(true)

        // layout manager, décrivant comment les items sont disposés :
        val layoutManager = LinearLayoutManager(this)
        recyclerViewVehicule.layoutManager = layoutManager

        // adapter :
        val vehiculesAdapter = VehiculesAdapter(listeVehicules)
        recyclerViewVehicule.adapter = vehiculesAdapter


    }
}